#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <malloc.h>

typedef struct cpu_data
{
    char _name[6];
    int _user;
    int _nicex;
    int _systemx;
    int _idle;
    int _iowait;
    int _irq;
    int _softirq;
    int _steal;
    int _guest;
    int _guest_nice;
};

typedef struct cpu_result
{
    char _name[6];
    float _percentage;
};

struct cpu_result calculate(struct cpu_data data1, struct cpu_data data2)
{
    struct cpu_result result;

    float PrevIdle = data2._idle + data2._iowait;
    float Idle = data1._idle + data1._iowait;
    float PrevNonIdle = data2._user + data2._nicex + data2._systemx + data2._irq + data2._softirq + data2._steal;
    float NonIdle = data1._user + data1._nicex + data1._systemx + data1._irq + data1._softirq + data1._steal;
    float PrevTotal = data2._idle + data2._iowait + PrevNonIdle;
    float Total = Idle + NonIdle;
    float totalId = Total - PrevTotal;
    float idled = Idle - PrevIdle;

    for (int i = 0; i < 6; i++)
    {
        result._name[i] = data1._name[i];
    }
    result._percentage = (totalId - idled) / totalId * 100;
    return result;
}

int main(int argc, char **argv)
{
    struct cpu_data data1 = {"cpu0", 693334, 50, 466600, 6483477, 22136, 0, 70099, 0, 0, 0};
    struct cpu_data data2 = {"cpu0", 694077, 50, 467138, 6492555, 22237, 0, 70243, 0, 0, 0};
    calculate(data1, data2);
}