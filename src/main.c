#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <ctype.h>
#include <malloc.h>
#include <time.h>
#include <semaphore.h>
#include <stdatomic.h>
#include "cpudata.h"
#include "cpu_result.h"
#include "reader.h"
#include "analyzer.h"
#include "writer.h"
#define NUM_THREADS 3 // Number of threads

// Declaring variables as volatile
static struct cpu_data *datax1;
static struct cpu_data *datax2;
static struct cpu_result *result;

// Declaring semaphores
static int flag = 1;
static int flag2 = 0;
static int flag3 = 0;

void handle_sigterm(int signum)
{
    flag = 0;
}

void *reader() // Thread to read
{
    while (flag)
    {
        sleep(1);
        load_data(datax1);
        // sleep(1); // Making sure that values datax1 and datax2 are different
        sleep(1);
        load_data(datax2);
        if (datax1 == datax2) // checking if values are different
        {
            perror("Datas are the same \n");
        }
        flag2 = 1;
    }
    return (void *)0;
}

void *analyzer() // Thread to calculate data
{
    while (flag)
    {
        if (flag2 == 1)
        {

            for (int i = 0; i < NOPT + 1; i++)
            {
                result[i] = calculate(&datax1[i], &datax2[i]); // calculating data
            }
            sleep(1); // Make sure that semaphores changes value
            flag3 = 1;
        }
    }
    return (void *)0;
}

void *writer() // thread to write data
{
    while (flag)
    {

        if (flag3 == 1)
        {

            writeresult(result);
            sleep(1); // Make sure that semaphores changes value
            printf("\n\n\n\n\n\n");
        }
    }
    return (void *)0;
}

int main()
{
    // Allocating memory for data
    datax1 = (struct cpu_data *)malloc((NOPT + 1) * sizeof(struct cpu_data));
    datax2 = (struct cpu_data *)malloc((NOPT + 1) * sizeof(struct cpu_data));
    result = (struct cpu_result *)malloc((NOPT + 1) * sizeof(struct cpu_result));
    pthread_t thread[NUM_THREADS]; // declaring threads

    // Handling Signal
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = handle_sigterm;
    sigaction(SIGTERM, &action, NULL);

    pthread_create(&thread[0], NULL, &reader, NULL);
    pthread_create(&thread[1], NULL, &analyzer, NULL);
    pthread_create(&thread[2], NULL, &writer, NULL);

    // make sure threads stopped
    for (int i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(thread[i], NULL);
        sleep(1);
    }

    // freeing memory
    free(datax1);
    free(datax2);
    free(result);

    return 0;
}