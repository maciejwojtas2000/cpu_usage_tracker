#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <malloc.h>
#include "cpudata.h"

#define NOPT get_nprocs_conf() //(number of processor thread) Liczba skonfigurowanych rdzeni procesora

int main(int argc, char **argv)
{

    struct cpu_data *data = (struct cpu_data *)malloc((NOPT + 1) * sizeof(struct cpu_data));

    FILE *file_stat = fopen("/proc/stat", "r");
    /*
        fscanf(file_stat, "%s %d %d %d %d %d %d %d %d %d %d", name, &user, &nicex, &systemx, &idle, &iowait, &irq, &softirq, &steal, &guest, &guest_nice);

        printf("WCZYTANE DANE %s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", name, user, nicex, systemx, idle, iowait, irq, softirq, steal, guest, guest_nice);
    */
    for (int i = 0; i < NOPT + 1; i++)
    {
        fscanf(file_stat, "%s %d %d %d %d %d %d %d %d %d %d",
               data[i]._name,
               &(data[i]._user),
               &(data[i]._nicex),
               &(data[i]._systemx),
               &(data[i]._idle),
               &(data[i]._iowait),
               &(data[i]._irq),
               &(data[i]._softirq),
               &(data[i]._steal),
               &(data[i]._guest),
               &(data[i]._guest_nice));

        printf("WCZYTANE DANE %s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d \n",
               (data[i]._name),
               (data[i]._user),
               (data[i]._nicex),
               (data[i]._systemx),
               (data[i]._idle),
               (data[i]._iowait),
               (data[i]._irq),
               (data[i]._softirq),
               (data[i]._steal),
               (data[i]._guest),
               (data[i]._guest_nice));
    }
    fclose(file_stat);
    free(data);
    return 0;
}