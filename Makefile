FLAGS= -pthread -Wall -Wextra -pedantic -std=c17 -iquote inc

__start__: ./a.out
	./a.out

./a.out: obj obj/main.o 

	gcc -pthread -o ./a.out obj/main.o

obj:
	mkdir obj

obj/main.o: src/main.c inc/cpudata.h inc/analyzer.h inc/reader.h inc/cpu_result.h inc/writer.h
	gcc -c ${FLAGS} -o obj/main.o src/main.c

test: ./test.out
	./test.out

./test.out: obj obj/test.o 

	gcc -pthread -o ./test.out obj/test.o

obj:
	mkdir obj

obj/test.o: src/test.c inc/cpudata.h inc/analyzer.h inc/reader.h inc/cpu_result.h inc/writer.h
	gcc -c ${FLAGS} -o obj/test.o src/test.c



clean:
	rm -f ./a.out ./test.out obj/*
