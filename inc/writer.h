#ifndef WRITER_H
#define WRITER_H

#include <stdio.h>
#include <stdlib.h>
#include "cpudata.h"
#include "cpu_result.h"

// delay
void delay(int number_of_seconds)
{
    int milli_seconds = 1000 * number_of_seconds; // Converting time into milli_seconds
    clock_t start_time = clock();                 // Storing start time
    while (clock() < start_time + milli_seconds)
        ; // looping till required time is not achieved
}

// function used for testing. it's writes struct cpu_data
void write_data(struct cpu_data *data)
{
    for (int i = 0; i < NOPT + 1; i++)
    {
        printf("READ DATA %s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d \n",
               data[i]._name,
               data[i]._user,
               data[i]._nicex,
               data[i]._systemx,
               data[i]._idle,
               data[i]._iowait,
               data[i]._irq,
               data[i]._softirq,
               data[i]._steal,
               data[i]._guest,
               data[i]._guest_nice);
    }
    printf("\n\n\n\n\n\n\n");
}

// function to write struct cpu_result
void writeresult(struct cpu_result *result)
{
    for (int i = 0; i < NOPT + 1; i++)
    {
        printf("Processor: %s,  Usage: %0.2f%% \n",
               result[i]._name,
               fabs(result[i]._percentage));
    }
}

#endif