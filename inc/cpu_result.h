#ifndef CPU_RESULT_H
#define CPU_RESULT_H

// structure to contain result of calculating data from /proc/stat
typedef struct cpu_result
{
    char _name[6];     // Name of the one of the processors. Ex. "cpu, cpu0, cpu1"
    float _percentage; // Usage of that processor in percentages
};

#endif