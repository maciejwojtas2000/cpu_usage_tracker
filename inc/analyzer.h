#ifndef ANALYZER_H
#define ANALYZER_H
#include <math.h>
#include "cpudata.h"
#include "cpu_result.h"

// Function calculating usage of processor(in percentages)
struct cpu_result calculate(struct cpu_data *data1, struct cpu_data *data2)
{
    struct cpu_result result;

    int PrevIdle = data2->_idle + data2->_iowait;
    int Idle = data1->_idle + data1->_iowait;
    int PrevNonIdle = data2->_user + data2->_nicex + data2->_systemx + data2->_irq + data2->_softirq + data2->_steal;
    int NonIdle = data1->_user + data1->_nicex + data1->_systemx + data1->_irq + data1->_softirq + data1->_steal;
    int PrevTotal = data2->_idle + data2->_iowait + PrevNonIdle;
    int Total = Idle + NonIdle;

    int totalId = Total - PrevTotal;
    int idled = Idle - PrevIdle;

    for (int i = 0; i < 6; i++)
    {
        result._name[i] = data1->_name[i];
    }
    result._percentage = (float)(totalId - idled) / (float)totalId * 100;
    return result;
}
#endif