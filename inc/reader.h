#ifndef READER_H
#define READER_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include "cpudata.h"
#define NOPT get_nprocs_conf() // NUMBER OF CONFIGURED PROCESSOR'S THREADS

// function to load data from /proc/stat to struct cpu_data
void load_data(struct cpu_data *data)
{
  FILE *file_stat = fopen("/proc/stat", "r"); // opening file

  if (file_stat == NULL)
  {
    printf("Error! Could not open file\n");
    exit(-1);
  }

  for (int i = 0; i < NOPT + 1; i++) // Read all lines which contain information about CPU
  {
    fscanf(file_stat, "%s %d %d %d %d %d %d %d %d %d %d",
           data[i]._name,
           &(data[i]._user),
           &(data[i]._nicex),
           &(data[i]._systemx),
           &(data[i]._idle),
           &(data[i]._iowait),
           &(data[i]._irq),
           &(data[i]._softirq),
           &(data[i]._steal),
           &(data[i]._guest),
           &(data[i]._guest_nice));
  }

  fclose(file_stat); // closing file
}

#endif