#ifndef CPUDATA_H
#define CPUDATA_H

// Defining structure to contain CPU's data from /proc/stat
typedef struct cpu_data
{
    char _name[6];   // name of the processor
    int _user;       // normal processes executing in user mode
    int _nicex;      // niced processes executing in user mode
    int _systemx;    // processes executing in kernel mode
    int _idle;       // twiddling thumbs
    int _iowait;     // waiting for I/O to complete
    int _irq;        // servicing interrupts
    int _softirq;    // servicing softirqs
    int _steal;      // “stolen” time from processor to run other operating systems in a virtualized environment
    int _guest;      // time spent on processes running on a virtual CPU with normal priority
    int _guest_nice; // time spent on processes running on a virtual CPU with niced priority
};

#endif